## Private Docker Repository

In this case we use gilap repository as a private repository. This is
the [course repository](https://gitlab.com/3a-tools/docker-course) in gitlab. inside this repo we can find the
[container registry](https://gitlab.com/3a-tools/docker-course/container_registry). To upload image to private
repository we need to know some naming conventions in for gitlab registry the naming is described
in [gilab docs](https://docs.gitlab.com/ee/user/packages/container_registry/index.html).

### create the image and upload.

First we neet to login in the repo using `docker login` command.

```bash
$ docker login registry.gitlab.com -u angosil
Password:
```

And typing the password. Now we can upload the images to this repository. To upload an image to registry different from
docker hub we need to specify the complete url in the image name. So we build again the image to upload to the gitlab
project repo.

```shell
$ docker build -t registry.gitlab.com/3a-tools/docker-course/mongoflask:0.1 .
```

Next steep, upload the new image to the private repository whit `docke push` command.

```shell
$ docker push registry.gitlab.com/3a-tools/docker-course/mongoflask:0.1
The push refers to repository [registry.gitlab.com/3a-tools/docker-course/mongoflask]
4c7b1f44d58c: Pushed
5f70bf18a086: Pushed
56bfc6ea1a95: Pushed
763c6c3c1ad5: Pushed
ec25cf848652: Pushed
6ad001c88f9c: Pushed
2a847769e19a: Pushed
af9068b69f74: Pushed
c1065d45b872: Pushed
0.1: digest: sha256:202f5b9fb8bfc75cb3b0038e719ab994b4d5edbf47055d26a3ba3287768366b9 size: 2202
```

Now we can check our private
repository [https://gitlab.com/3a-tools/docker-course/container_registry](https://gitlab.com/3a-tools/docker-course/container_registry)
