## Main commands

### Overview

- [ ] Difference between Image and Container.
- [ ] Version and Tag.
- [ ] Docker Commands.
    - [ ] docker pull
    - [ ] docker images
    - [ ] docker run
    - [ ] docker ps
    - [ ] docker stop
    - [ ] docker start
    - [ ] docker exec -it

### Difference between image and container.

Basically the difference is, how I said before. The image is the artifact, the package. The container is the image
running in on specific environment. We can see this in next moments whit some commands.

### Docker Commands.

We can start running the command `docker pull`. This coomand download one imagen from [Docker](https://docs.docker.com/)
, wish is the official public repository.

```shell
$ docker pull postgres:13.6-alpine
13.6-alpine: Pulling from library/postgres
40e059520d19: Pull complete
f7cb04fc0996: Pull complete
dd3fde6b0ba8: Pull complete
78e59ff0a5aa: Pull complete
a8f92b621fe0: Pull complete
88b0572c74cb: Pull complete
e275b2bc844c: Pull complete
6664777a1811: Pull complete
Digest: sha256:186db15c405dc40f22fbb0dce590f15aa8d202fad49d50d8a7368aa47842fcad
Status: Downloaded newer image for postgres:13.6-alpine
docker.io/library/postgres:13.6-alpine
```

we download the image for postgres with the tag 13.6-alpine. The tag is used to identified different versions of the
same application or use to identify the architect of the image. We try to download a second image for postgres use the
same command.

```shell
$ docker pull postgres:14.2-alpine
14.2-alpine: Pulling from library/postgres
40e059520d19: Already exists
f7cb04fc0996: Already exists
dd3fde6b0ba8: Already exists
21145cacff6e: Pull complete
4254b8496101: Pull complete
e683698d4515: Pull complete
cb491cf763d7: Pull complete
535f29d1b43a: Pull complete
Digest: sha256:d3b857fac70936a2e0dd83226cc113db1787a4f6119df77a798d8137354c4989
Status: Downloaded newer image for postgres:14.2-alpine
docker.io/library/postgres:14.2-alpine
```

With those commands we download two different postgres images. Now I can see or find the images that I'm already
downloaded. So with the command `docker ps` I can see all the images that are in my local machine.

```shell
docker images
REPOSITORY   TAG           IMAGE ID       CREATED        SIZE
postgres     13.6-alpine   f87d43a2bc65   29 hours ago   207MB
postgres     14.2-alpine   114818c12d10   29 hours ago   211MB
```

Those images are package. What is a container? if a container is a running image we try to run a container with one of
these images. To do that we use the command `docker run`

```shell
$ docker run -e POSTGRES_PASSWORD=root postgres:14.2-alpine
```

This command run a container using postgres images in our case. Here I use the flag `-e` this flag allows passing env
var to the container and this is one of the way that we can set up our application. I have now
a [Docker](https://docs.docker.com) imagen of psogres runing in my local machine. Who I can see? with the
command `docker ps`

```shell
$ docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED         STATUS         PORTS      NAMES
14431a993251   postgres:13.6-alpine   "docker-entrypoint.s…"   5 minutes ago   Up 5 minutes   5432/tcp   blissful_blackwell
590279ba7776   postgres:14.2-alpine   "docker-entrypoint.s…"   6 minutes ago   Up 6 minutes   5432/tcp   kind_almeida
```

With this command we can see all container that are running on our system. We can see here the difference between images
and containers now. The image is the package tha use to up the application, the column IMAGE and the container is the
running application, the column NAMES. We can run containers in detach, background. With the flag `-d` in `docker run`
command.

```shell
$ docker run -e POSTGRES_PASSWORD=root -d postgres:14.2-alpine
92c0506cada0ad5c5ad42d2525ac15e642873a422cf0d016d50b85c6fa5b0bb2
```

In this case the output is mose simple only the container id. I need now the way to manage the container that are
running in my system. For example if the docker container crashes and I want to stop the command is `docker stop`. To
stop we can use the CONTAINER ID or NAMES of the container, as our choose.

```shell
$ docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED         STATUS         PORTS      NAMES
629e6bd67a54   postgres:14.2-alpine   "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   5432/tcp   gallant_heyrovsky
$ docker stop gallant_heyrovsky
crazy_hopper
```

So when I run a container I use the image and the configurations and if the container I tdo not loss those
configurations. If want to reuse this stopped container we need list of historical docker running. the command
is `docker ps` with the flag `-a`, Next we can use the command `docker start` to start the container.

```shell
$ docker ps -a
CONTAINER ID   IMAGE                  COMMAND                  CREATED             STATUS                         PORTS     NAMES
629e6bd67a54   postgres:14.2-alpine   "docker-entrypoint.s…"   About an hour ago   Exited (0) 8 seconds ago                 gallant_heyrovsky
eff3be2163c3   postgres:14.2-alpine   "docker-entrypoint.s…"   About an hour ago   Exited (0) About an hour ago             elastic_hellman
38ca55e0e2d3   postgres:14.2-alpine   "docker-entrypoint.s…"   About an hour ago   Exited (1) 39 seconds ago                angry_fermi
$ docker start gallant_heyrovsky
gallant_heyrovsky
$ docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED             STATUS         PORTS      NAMES
629e6bd67a54   postgres:14.2-alpine   "docker-entrypoint.s…"   About an hour ago   Up 2 seconds   5432/tcp   gallant_heyrovsky              angry_fermi             angry_fermi
```

In this command is possible to use CONTAINER ID or NAMES.

### Containers port vs Host port.

How I communicate with the container? How I use the container? One of the ways is using the port number. To use the port
of the container I need to map, or binding, the container port to the host port. How I do that? with the flag `-p` in
the command `docker run`. First I use the host port and then the docker container port.

```shell
docker run -p 5432:5432  -e POSTGRES_PASSWORD=postgres -d postgres:14.2-alpine
89d5c7637dd06957e1b20abe5c8d9cd0bde6fb1d323e450f8d75e8bfaa664011
```

With this command I bind the host port 5432 with the container port 5432. Now I can connect to this docker by postgres
cli client.

```shell
$ psql postgresql://postgres:postgres@localhost:5432/postgres
psql (12.9 (Ubuntu 12.9-0ubuntu0.20.04.1), server 14.2)
WARNING: psql major version 12, server major version 14.
         Some psql features might not work.
Type "help" for help.

postgres=#
```

If I want to start other postgres version at the same time. I need to binding other port of my host to the second
postgrest container. Because the port 5432 is occupied by the firth postgrest container. I will use the port 5433 in my
local host.

```shell
$ docker run -p 5433:5432  -e POSTGRES_PASSWORD=postgres -d postgres:13.6-alpine
b889230f1e72f6b9c01b9e8162fc48d04acd4b9d751f6dfda1630d626add7b23
```

I connect to this second server changing the port for the connection.

```shell
$ psql postgresql://postgres:postgres@localhost:5433/postgres
psql (12.9 (Ubuntu 12.9-0ubuntu0.20.04.1), server 13.6)
WARNING: psql major version 12, server major version 13.
         Some psql features might not work.
Type "help" for help.

postgres=#
```

If I want to enter on the container and run command inside. To set up som config or edit some files. The command to do
that is `docker exec -it`. This command allow to us interact with the container.

```shell
$ docker exec -it b889230f1e72 bash
bash-5.1#
```
