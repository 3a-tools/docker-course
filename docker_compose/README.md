## Docker Compose - Running multiple services

So far we set up one realistic develop environment using docker. But this must be very tedious because we neet to
launch, run stop star many containers separately, and we need to set up many configurations. Those command and
configuration can be tedious. Exist som tool tha allow os wires all those docker configurations in one file and run more
easily, [Docker Compose](https://docs.docker.com/compose/).

### Installation

If for window or mac the installation of docker desktop coming
with [docker compose](https://www.docker.com/products/docker-desktop/). Usually we need
to [install](https://docs.docker.com/compose/install/) on linux server in our case, as I mentioned above, ubuntu server.

### How to create docker-compose.yml file

To use a docker compose tool we need to create a single `yaml` file with all configuration tha we need for this project.
So our commands are.

```shell
$ docker network create mg-net
a874dd5dfd0f0a18f27e479b0414b7c081f70785c997d12f5c24e5e380eb9f6d
$ docker run -p 27017:27017 --name mgdb -d -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=admin --network mg-net mongo:5.0.6
Unable to find image 'mongo:5.0.6' locally
...
31676add92062aefdfaa69f0a44ce9d3e172b1de65f3c1b70dbcca8403d8b58f
$ docker run -d -p 8081:8081 --name mongo-express --network mg-net -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=admin -e ME_CONFIG_MONGODB_SERVER=mgdb mongo-express
...
2992eff3d1128190a217fc179a6fbe48c62caf6188f2c943ce303d76b6060f60
```

I try to map those commands to single `docker-compose.yaml` file

```yaml
version: '3'

services:
  mgdb:
    image: mongo:5.0.6
    ports:
      - "27017:27017"
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=admin
  mongo-express:
    image: mongo-express
    ports:
      - "8081:8081"
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=admin
      - ME_CONFIG_MONGODB_SERVER=mgdb
    depends_on:
      - mgdb
```

This is the basic configuration to run those services with one file. Interesting here is the network flag I do not use
because [docker compose](https/docs.docker.com/compose/) create newone for those services automatically. Now I will try
to use the same application [mongo flask app] but in this time I use docker compose.

### Running services with docker compose.

We need to enter to the project directory, [mongo_flask]. And then run the command `docker-compose up`. First we can
check if not container running.

```shell
$ docker ps
CONTAINER ID   IMAGE     COMMAND   CREATED   STATUS    PORTS     NAMES
```

Now we can run the docker `compose up` command whit the flag `-f` which is used to specific the docker compose file.

```shell
$ docker-compose -f mongo.yaml up
```

We have a lot of log here, but we can see downloaded images, network create, containers created and the containers
running with their logs. This is very useful because in one single command and with single configuration file we can set
up all mongo environment. Nok we can check [http://localhost:8081/](http://localhost:8081/).

```shell
$ docker network ls
NETWORK ID     NAME                  DRIVER    SCOPE
1b4b6d7cc193   bridge                bridge    local
33ce3a17ab9c   host                  host      local
a874dd5dfd0f   mg-net                bridge    local
5f9e425ab6c6   mongo_flask_default   bridge    local
30be6572ba62   none                  null      local
```

we can se the network `mongo_flask_default`.

```shell
$ docker ps
CONTAINER ID   IMAGE           COMMAND                  CREATED          STATUS         PORTS                      NAMES
5b1870ac3aa8   mongo-express   "tini -- /docker-ent…"   3 minutes ago    Up 3 minutes   0.0.0.0:8081->8081/tcp     mongo_flask-mongo-express-1
ec8c59e0080e   mongo:5.0.6     "docker-entrypoint.s…"   15 minutes ago   Up 3 minutes   0.0.0.0:27017->27017/tcp   mongo_flask-mgdb-1
```

And both container `mongo_flask-mongo-express-1` and `mongo_flask-mgdb-1` running. Now we set up our app as the same
steeps as in the previous chapter.

### Setup our flask application.

Enter to the mongo container with the docker compose command `docker-compose exec -it` with is similar to
the `docker exec` command. We need to use the flag `-f` and specify the service name.

```shell
$ docker-compose -f mongo.yaml exec -it mgdb bash
root@ec8c59e0080e:/#
```

Now I run the mongo client inside the container to crete new database, **users** and ad the user **admin** and some data
to this database.

```shell
root@37c04bfb4a9c:/# mongosh -u admin -p admin
flask_app> use flask_app
flask_app> db.users.insertMany([{name:"Many", age:25}, {name:"Sid", age:24}, {name:"Diego", age:23}, {name:"Ellie", age:22}])
flask_app> show databases
flask_app> db.createUser({user: "admin", pwd: "admin", roles:[{role: "readWrite", db: "flask_app"}]})
```

Set up python virtual environment.

```shell
$ python3.10 -m venv venv_l
$ source venv_l/bin/activate
(venv_l)
$ python -m pip install -r requirements.txt
$ export FLASK_APP=main
$ export FLASK_ENV=development
$ flask run
 * Serving Flask app 'main' (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 103-757-865
```

### Commands to work with docker compose.

Similar to docker command we can use the flag `-d` and the command `docker-compose log` to detach and log with docker
compose

```shell
$ docker-compose -f mongo.yaml up -d
[+] Running 2/2
 ⠿ Container mongo_flask-mgdb-1           Started                                                                                                      0.5s
 ⠿ Container mongo_flask-mongo-express-1  Started
```

the logs.

```shell
$ docker-compose -f mongo.yaml logs 
```

We can also specify the service to see only one service logs. To see the lis of services running
with `docker-compose -f mongo.yaml ps`.

```shell
$ docker-compose -f mongo.yaml ps
NAME                          COMMAND                  SERVICE             STATUS              PORTS
mongo_flask-mgdb-1            "docker-entrypoint.s…"   mgdb                running             0.0.0.0:27017->27017/tcp
mongo_flask-mongo-express-1   "tini -- /docker-ent…"   mongo-express       running             0.0.0.0:8081->8081/tcp
```

To remove all containers and networks we use `docker-compose -f mongo.yaml down`.

```shell
$ docker-compose -f mongo.yaml down
[+] Running 3/3
 ⠿ Container mongo_flask-mongo-express-1  Removed                                                                                                      0.4s
 ⠿ Container mongo_flask-mgdb-1           Removed                                                                                                      0.4s
 ⠿ Network mongo_flask_default            Removed                                                                                                      0.2s
```
