## Volumes - Persisting data.

So I try to introduce one consept [Docker Volumes].

### Overview

- [ ] When do we need Docker Volumes?
- [ ] What is Docker volumes?
- [ ] 3 volumes type.
- [ ] Docker Volume in docker-compose file.

### When do we need Docker Volumes?

When we use a database or any statefullset application, we need a volume to persis data.

- why? because if the container restart or rebuild, then star from fresh state.

We see this if we down or current application. thhis action stop and remove the containers

```shell
$ docker-compose -f mongo_flask_app.yaml down
[+] Running 4/4
 ⠿ Container mongo_flask-mongo-express-1  Removed                                                                                                                                                                                                                                  3.4s
 ⠿ Container mongo_flask-app-1            Removed                                                                                                                                                                                                                                  3.5s
 ⠿ Container mongo_flask-mgdb-1           Removed                                                                                                                                                                                                                                  0.5s
 ⠿ Network mongo_flask_default            Removed
```

When I start again. the application do not have any data that previously I add. How I solve this? with volumes.

### What is Docker volumes?

Is a way to mount data in the host system that are present in the container. This allows storage data or files directly
in the host filesystem and use in the container.

### 3 volumes type.

- host volume: `docker run -v /path/on/host:/path/in/container`. We can specify the host path to storge the data and map
  or bind to some path in the container.
- named volumes: `docker volume create somevolumename`, and `docker run -v name:/path/in/container`. We can not specify
  the host path but the volume have a name and i can refer to this name to manage the data or the volume in self.
- anonymous volumes: `docker run -v /path/in/container`. We can not specify the host path, only the path to the
  container. In this case docker storage the data in the host filesystem but without name is flexible but hart to
  manage.

### Docker Volume in docker-compose file.

How I manage those consept in docker-compose? I add a volume to the docker-compose file. Go
to [mongo docker](https://hub.docker.com/_/mongo) doc.

```yaml
version: '3'

services:
  mgdb:
    image: mongo:5.0.6
    ports:
      - "27017:27017"
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=admin
    volumes:
      - mgdb-data:/data/db
  mongo-express:
    image: mongo-express
    ports:
      - "8081:8081"
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=admin
      - ME_CONFIG_MONGODB_SERVER=mgdb
    depends_on:
      - mgdb
  app:
    image: registry.gitlab.com/3a-tools/docker-course/mongoflask:0.1
    ports:
      - "5000:5000"
    environment:
      - MG_SERVER=mgdb
    depends_on:
      - mgdb

volumes:
  mgdb-data: {}
```

I need to add the new user and DB.

```shell
$ docker-compose -f mongo_flask_app.yaml exec -it mgdb bash
root@37c04bfb4a9c:/# mongosh -u admin -p admin
flask_app> use flask_app
flask_app> db.users.insertMany([{name:"Many", age:25}, {name:"Sid", age:24}, {name:"Diego", age:23}, {name:"Ellie", age:22}])
flask_app> show databases
flask_app> db.createUser({user: "admin", pwd: "admin", roles:[{role: "readWrite", db: "flask_app"}]})
```

Now we can test if work fine, and down again. 

```shell
$ docker-compose -f mongo_flask_app.yaml down
[+] Running 4/4
 ⠿ Container mongo_flask-app-1            Removed                                                                                                                                                                                                                                  2.5s
 ⠿ Container mongo_flask-mongo-express-1  Removed                                                                                                                                                                                                                                  0.4s
 ⠿ Container mongo_flask-mgdb-1           Removed                                                                                                                                                                                                                                  0.4s
 ⠿ Network mongo_flask_default            Removed
```

and start again, and we can see the data in the system.

```shell
$ docker-compose -f mongo_flask_app.yaml up -d
[+] Running 4/4
 ⠿ Network mongo_flask_default            Created                                                                                                                                                                                                                                  0.0s
 ⠿ Container mongo_flask-mgdb-1           Started                                                                                                                                                                                                                                  2.6s
 ⠿ Container mongo_flask-app-1            Started                                                                                                                                                                                                                                  1.4s
 ⠿ Container mongo_flask-mongo-express-1  Started
```
