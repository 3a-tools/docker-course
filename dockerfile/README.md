## Dockerfile - Build your own Docker image.

To deploy our application using docker images we need to create one. How I do that? with some file that is blueprint to
create images, the Dockerfile. We need to create or application Dockerfile.

```Dockerfile
FROM python:3.10.4-slim-buster

ENV FLASK_APP=main \
    FLASK_ENV=development \
    HOME_APP=/home/app

RUN mkdir -p $HOME_APP

COPY . $HOME_APP

WORKDIR $HOME_APP

RUN python -m pip install -r requirements.txt

CMD ["flask", "run", "-h", "0.0.0.0"]

```

- Set the image in wish our app is based `FROM python:3.10.4-slim-buster`. We can find the
  one [python official images](https://hub.docker.com/_/python) from docker hub.
- Add some environment variables, the application need to work properly. With the work `ENV` we can add environment vars
  to the image.
- Create the application home directory. To do that I use the `RUN` keyword. With this word we can run almost run every
  linux command.
- Copy all of our applications files into the home directory of the images with `COPY`.
- Change the working directory. this is like cd command.
- Run other command, in this case is to install python dependencies to our application.
- Finally, we star our flask app, `CMD`, with this command we run our final command for the image.

### creating and testing our app.

We need to create our image for the application running the command `docker build`. With the flag `-t` we can specify
the name and tag for the image. The dot is to use the actual directory to find the `Dockerfile` file to build the
imagen.

```bash
$ docker build -t mongo-flask:0.1 .
[+] Building 3.6s (11/11) FINISHED
 => [internal] load build definition from Dockerfile                                                                                                   0.0s
 => => transferring dockerfile: 38B                                                                                                                    0.0s
 => [internal] load .dockerignore                                                                                                                      0.0s
 => => transferring context: 2B                                                                                                                        0.0s
 => [internal] load metadata for docker.io/library/python:3.10.4-slim-buster                                                                           3.4s
 => [auth] library/python:pull token for registry-1.docker.io                                                                                          0.0s
 => [1/5] FROM docker.io/library/python:3.10.4-slim-buster@sha256:30a3b78e3ea71e4b4613ef9eb849f62ec9172a009cf01948ee659097526fb1d6                     0.0s
 => [internal] load build context                                                                                                                      0.1s
 => => transferring context: 606B                                                                                                                      0.0s
 => CACHED [2/5] RUN mkdir -p /home/app                                                                                                                0.0s
 => CACHED [3/5] COPY . /home/app                                                                                                                      0.0s
 => CACHED [4/5] WORKDIR /home/app                                                                                                                     0.0s
 => CACHED [5/5] RUN python -m pip install -r requirements.txt                                                                                         0.0s
 => exporting to image                                                                                                                                 0.0s
 => => exporting layers                                                                                                                                0.0s
 => => writing image sha256:5f679865a0d2904ee8732ae940397b90c2ab6fc3b59b818602cc7207ac4f7af9                                                           0.0s
 => => naming to docker.io/library/mongo-flask:0.1
```

Now we have the image locally. Can we check with the command `docker images`.

```bash
$ docker images
REPOSITORY    TAG       IMAGE ID       CREATED          SIZE
mongo-flask   0.1       5f679865a0d2   10 minutes ago   133MB
```

To test we can run the container with `docker run` and test if running.

```bash
$ docker run -d -p 5000:5000 mongo-flask:0.1
e452e393d7d99e8836d90d9f4dc88a6d211a82c66fbf3a979624d2c0bbb34310
```

`docker ps` to see if the container is running.

```bash
$ docker ps 
CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES 
e452e393d7d9 mongo-flask:0.1   "flask run -h 0.0.0.0"
```

`docker log` to debug if the image is working.

```bash
$ docker logs e452e393d7d9
* Serving Flask app 'main' (lazy loading)
* Environment: development
* Debug mode: on
* Running on all addresses (0.0.0.0)
  WARNING: This is a development server. Do not use it in a production deployment.
* Running on http://127.0.0.1:5000
* Running on http://172.17.0.2:5000 (Press CTRL+C to quit)
* Restarting with stat
* Debugger is active!
* Debugger PIN: 141-933-789
```

Now we have a local image we neet to upload to a private directory. 