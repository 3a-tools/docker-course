## Application deploy

So far we can create develop and application, create image for this application, upload the image to private repository
and now we neet ptu use. To do that we use a docker compose file to manage all or environment.

```yaml
version: '3'

services:
  mgdb:
    image: mongo:5.0.6
    ports:
      - "27017:27017"
    environment:
      - MONGO_INITDB_ROOT_USERNAME=admin
      - MONGO_INITDB_ROOT_PASSWORD=admin
  mongo-express:
    image: mongo-express
    ports:
      - "8081:8081"
    environment:
      - ME_CONFIG_MONGODB_ADMINUSERNAME=admin
      - ME_CONFIG_MONGODB_ADMINPASSWORD=admin
      - ME_CONFIG_MONGODB_SERVER=mgdb
    depends_on:
      - mgdb
  app:
    image: registry.gitlab.com/3a-tools/docker-course/mongoflask:0.1
    ports:
      - "5000:5000"
    environment:
      - MG_SERVER=mgdb
    depends_on:
      - mgdb
```

I only add our new image to the docker compose that actually I have, and now we can run the commands that already
know `docker-compose up`

```bash
$ docker-compose -f mongo_flask_app.yaml up -d
[+] Running 3/3
 ⠿ Container mongo_flask-mgdb-1           Running                                                                                                      0.0s
 ⠿ Container mongo_flask-app-1            Running                                                                                                      0.0s
 ⠿ Container mongo_flask-mongo-express-1  Started
```

We check with the command `docker ps`.

```bash
$ docker ps
CONTAINER ID   IMAGE                                                       COMMAND                  CREATED          STATUS          PORTS
    NAMES
ac8fecbe3160   mongo-express                                               "tini -- /docker-ent…"   30 seconds ago   Up 7 seconds    0.0.0.0:8081->8081/tcp     mongo_flask-mongo-express-1
209555e6f52f   registry.gitlab.com/3a-tools/docker-course/mongoflask:0.1   "flask run -h 0.0.0.0"   30 seconds ago   Up 26 seconds   0.0.0.0:5000->5000/tcp     mongo_flask-app-1
3f2b70c74085   mongo:5.0.6                                                 "docker-entrypoint.s…"   31 seconds ago   Up 28 seconds   0.0.0.0:27017->27017/tcp   mongo_flask-mgdb-1
```

Now we can check if our app is working. [http://localhost:5000/](http://localhost:5000/), we see the same error that
already know. I need to add the new user and DB.

```shell
$ docker-compose -f mongo_flask_app.yaml exec -it mgdb bash
root@37c04bfb4a9c:/# mongosh -u admin -p admin
flask_app> use flask_app
flask_app> db.users.insertMany([{name:"Many", age:25}, {name:"Sid", age:24}, {name:"Diego", age:23}, {name:"Ellie", age:22}])
flask_app> show databases
flask_app> db.createUser({user: "admin", pwd: "admin", roles:[{role: "readWrite", db: "flask_app"}]})
```

So far we have a full deploy solution. 
