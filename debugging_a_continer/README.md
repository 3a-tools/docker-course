## Debugging a container.

- [ ] Docker Commands.
    - [ ] docker exec -it
    - [ ] docker logs

If I can't connect to my postgres server, in this case was useful see the logs in of the application. The command to see
the logs of a container is `docker logs`.

```shell
$ docker logs ecstatic_bohr
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

...

2022-03-30 11:18:40.539 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2022-03-30 11:18:40.547 UTC [49] LOG:  database system was shut down at 2022-03-30 11:18:40 UTC
2022-03-30 11:18:40.552 UTC [1] LOG:  database system is ready to accept connections
```

Now if I want to view the virtual file structure or some value for some environment variables inside the container, we
can use the command `docker exet -it` .

```shell
$ docker exec -it b889230f1e72 bash
bash-5.1#
```

and inside the container run our commands.

```shell
bash-5.1# ls -lha
total 72K
drwxr-xr-x    1 root     root        4.0K Mar 30 11:18 .
drwxr-xr-x    1 root     root        4.0K Mar 30 11:18 ..
-rwxr-xr-x    1 root     root           0 Mar 30 11:18 .dockerenv
drwxr-xr-x    1 root     root        4.0K Mar 29 04:45 bin
drwxr-xr-x    5 root     root         340 Mar 30 11:18 dev
drwxr-xr-x    2 root     root        4.0K Mar 29 04:35 docker-entrypoint-initdb.d
drwxr-xr-x    1 root     root        4.0K Mar 30 11:18 etc
drwxr-xr-x    2 root     root        4.0K Mar 28 19:25 home
drwxr-xr-x    1 root     root        4.0K Mar 29 04:45 lib
drwxr-xr-x    5 root     root        4.0K Mar 28 19:25 media
drwxr-xr-x    2 root     root        4.0K Mar 28 19:25 mnt
drwxr-xr-x    2 root     root        4.0K Mar 28 19:25 opt
dr-xr-xr-x  211 root     root           0 Mar 30 11:18 proc
drwx------    2 root     root        4.0K Mar 28 19:25 root
drwxr-xr-x    1 root     root        4.0K Mar 29 04:45 run
drwxr-xr-x    1 root     root        4.0K Mar 29 04:45 sbin
drwxr-xr-x    2 root     root        4.0K Mar 28 19:25 srv
dr-xr-xr-x   11 root     root           0 Mar 30 11:18 sys
drwxrwxrwt    1 root     root        4.0K Mar 29 04:45 tmp
drwxr-xr-x    1 root     root        4.0K Mar 29 04:45 usr
drwxr-xr-x    1 root     root        4.0K Mar 29 04:45 var
```

```bash
bash-5.1# env
HOSTNAME=b889230f1e72
POSTGRES_PASSWORD=postgres
PWD=/
PG_SHA256=bafc7fa3d9d4da8fe71b84c63ba8bdfe8092935c30c0aa85c24b2c08508f67fc
HOME=/root
LANG=en_US.utf8
PG_MAJOR=13
PG_VERSION=13.6
TERM=xterm
SHLVL=1
PGDATA=/var/lib/postgresql/data
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
_=/usr/bin/env

```

### The name of the containers

something usefully here is put specific name to the container this can be done use the flag `--name` in the command
`docker run`.

```shell
$ docker run --name pg13 -p 5434:5432  -e POSTGRES_PASSWORD=postgres -d postgres:13.6-alpine
af987a1627bbc2b677765991143bca4d2ea66feff2555ea384b41d0f73f5972f
```

And we can remember this image to connect and debug if I need.
