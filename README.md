# Docker Course

## Course Content

- [ ] What is Docker? What is a Container?
- [ ] [Docker](https://docs.docker.com/) vs Virtual Machine.
- [ ] [Docker](https://docs.docker.com/) installation.
- [ ] [Main commands](main_comands).
- [ ] [Debugging a container](debugging_a_continer).
- [ ] [Developing with container](developing).
- [ ] [Docker Compose - Running multiple services](docker_compose).
- [ ] [Dockerfile - Build your own Docker image](dockerfile).
- [ ] [Private Docker Repository](repository).
- [ ] [Application deploy](deploying)
- [ ] [Volumes - Persisting data](volumes).

## What is Docker? What is a Container?

### Principals topics, Overview

- [ ] What is Docker and what problems it solves?
- [ ] Container repository
- [ ] Develop application
- [ ] Deploy application

### What is Docker?

- Is a way to package applications with all the necessary dependencies and configuration.
- With Docker, you can Build portable artifact, images and easily shared and move around.
- This way of packaging the application makes development and deployment more efficient.

### container repository

Container are portable, so we need some storage for this artifact. The container repository, is the place where
container are storage.

- private repository
- public repository. [Docker Hub](https://hub.docker.com/)

[Docker Hub](https://hub.docker.com/) is a public repository, we can find here probably any application tha we need to
use. This is probably the place where you start finding Docker applications.

### How container improved the application development experience.

Without container: Installation of every binary application in our respective operating system. That can be different,
Windows or Linux or MacOS, and we also need different configurations to.

- different installation in each environment
- many configurations steps in t move between different environments

Whit container: We do not need tuo install any binary directly in oun system. We only need to find the application that
we need for to develop and download.

- same installation process
- same configuration steps

### How container improved the application deployment experience.

Without container: Every developer create his own artifact for the applications, zip, exe, binary and the espesific
configuration for the server.

- dependency version conflict.
- misunderstanding between developers and operations.

Whit container: Developers and operations work together to package the applications in a container. The developer only
need to push the container to repository.

- docker running on server

### What is a Container?

- Layers of images on top of each other.
- Mostly linux based images, like alpine or debian-slim.
- Each layer is minimal because we want a minimal size.
- In the bottom we have a linux base image and in top the final application layer.

We can see this running the command to download an image. For example, we can download a specific version of postgres.
We only need to use the command `docker pull` this download one imagen from [Docker](https://docs.docker.com/), wish is
the official public repository.

```shell
$ docker pull postgres:14.2-alpine
14.2-alpine: Pulling from library/postgres
40e059520d19: Pull complete
f7cb04fc0996: Pull complete
dd3fde6b0ba8: Pull complete
21145cacff6e: Pull complete
4254b8496101: Pull complete
e683698d4515: Pull complete
cb491cf763d7: Pull complete
535f29d1b43a: Pull complete
Digest: sha256:d3b857fac70936a2e0dd83226cc113db1787a4f6119df77a798d8137354c4989
Status: Downloaded newer image for postgres:14.2-alpine
docker.io/library/postgres:14.2-alpine
```

[Docker](https://docs.docker.com/) try to get image locally and then, if not present, download
from [Docker Hub](https://hub.docker.com/). We can see the different layers that form this container
image. [Docker](https://docs.docker.com/) download all layers for specific image but if we try pull a second of other
version of the same software we can see that image shared some layers. [Docker](https://docs.docker.com/) download only
the layers that are not present.

```shell
$ docker pull postgres:13.6-alpine
13.6-alpine: Pulling from library/postgres
40e059520d19: Already exists
f7cb04fc0996: Already exists
dd3fde6b0ba8: Already exists
78e59ff0a5aa: Pull complete
a8f92b621fe0: Pull complete
88b0572c74cb: Pull complete
e275b2bc844c: Pull complete
6664777a1811: Pull complete
Digest: sha256:186db15c405dc40f22fbb0dce590f15aa8d202fad49d50d8a7368aa47842fcad
Status: Downloaded newer image for postgres:13.6-alpine
docker.io/library/postgres:13.6-alpine
```

We can now run a container using see the command `docker run` use some image downloaded.

```shell
$ docker run -e POSTGRES_PASSWORD=admin postgres:14.2-alpine
The files belonging to this database system will be owned by user "postgres".
This user must also own the server process.

The database cluster will be initialized with locale "en_US.utf8".
The default database encoding has accordingly been set to "UTF8".
The default text search configuration will be set to "english".

...

2022-03-31 10:37:45.024 UTC [1] LOG:  starting PostgreSQL 14.2 on x86_64-pc-linux-musl, compiled by gcc (Alpine 10.3.1_git20211027) 10.3.1 20211027, 64-bit
2022-03-31 10:37:45.024 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
2022-03-31 10:37:45.024 UTC [1] LOG:  listening on IPv6 address "::", port 5432
2022-03-31 10:37:45.033 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2022-03-31 10:37:45.042 UTC [48] LOG:  database system was shut down at 2022-03-31 10:37:44 UTC
2022-03-31 10:37:45.047 UTC [1] LOG:  database system is ready to accept connections
```

We can see different logs that show the info of application starting up. And now we can run `docker ps` this command
show the container tha are already running.

```shell
$ docker ps
CONTAINER ID   IMAGE                  COMMAND                  CREATED         STATUS         PORTS      NAMES
629e6bd67a54   postgres:14.2-alpine   "docker-entrypoint.s…"   4 minutes ago   Up 4 minutes   5432/tcp   gallant_heyrovsky
```

### Docker image vs Docker Container

- Docker image is the package. the artifact tha is storage.
- Docker Container is the imagen pulled and installed in a local environment.

## Docker vs Virtual Machine.

### What is the difference between [Docker](https://docs.docker.com/) and Virtual Machines?

[Docker](https://docs.docker.com/) works in the operating system.

The operating system have two layers.

- kernel
- application

Docker and Virtual Machine are both virtualization tools. What is the level of virtualization?

- [Docker](https://docs.docker.com) virtualized the application layer. Each docker application use OS kernel layer and
  run in the application layer.
- Virtual Machine virtualized the OS kernel layer. Each application run on Virtual Machine run on it own kernel layer.

One of te implication is the size. The Virtual machine are mor bigger because they have a complete operating system. For
the other hand, the [Docker](https://docs.docker.com) use the Operating System, run as an application.

Second is the speed. Because the virtual machine need to run all operating system is slower than Docker.

## Docker installation.

Here we find all instructions for [docker installation](https://docs.docker.com/engine/install/). I only try to explain
the linux installation, in our case
[Ubuntu Docker](https://docs.docker.com/engine/install/ubuntu/).
