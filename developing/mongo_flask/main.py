import json

from bson import ObjectId
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo


class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(self, o)


app = Flask(__name__)
app.json_encoder = JSONEncoder
app.config["MONGO_URI"] = "mongodb://admin:admin@localhost:27017/flask_app"
mongo = PyMongo(app)


@app.route("/", methods=["GET"])
def get_users():
    online_users = mongo.db.users.find({})
    return jsonify([i for i in online_users])


@app.route("/", methods=["POST"])
def set_users():
    content = request.json
    inserted_id = mongo.db.users.insert_one(content).inserted_id
    new_user = mongo.db.users.find_one({"_id": inserted_id})
    return jsonify(new_user)
