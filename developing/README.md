## Developing with container

One of the scenarios tha we can use [docker](https://docs.docker.com) to develop an applications is. I develop the app
locally and use different dockers to interact. For example if I need to create some Python Flask app, and I need to
storage the data in some db. I will run some db docker image for example [mongo](https://hub.docker.com/_/mongo).

So we start preparing our environment downloading [mongo](https://hub.docker.com/_/mongo)
and [mongo expless](https://hub.docker.com/_/mongo-express) which is the web ui
for [mongo](https://hub.docker.com/_/mongo). Is important to know here the docker network. Because we need
communicate [mongo](https://hub.docker.com/_/mongo) with their ui.

### Docker Networks

Docker can create it onw isolate network. If I define a docker network, and I use to run docker container this container
can communicate using the container name.

We can list all created networks with the command `docker network ls`.

```shell
$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
1b4b6d7cc193   bridge    bridge    local
33ce3a17ab9c   host      host      local
30be6572ba62   none      null      local
```

Now I can create the new network for to establish the communications and between [mongo](https://hub.docker.com/_/mongo)
db and [mongo expless](https://hub.docker.com/_/mongo-express). We can use the command `docker network create`.

```shell
$ docker network create mg-net
a874dd5dfd0f0a18f27e479b0414b7c081f70785c997d12f5c24e5e380eb9f6d
$ docker network ls
NETWORK ID     NAME      DRIVER    SCOPE
1b4b6d7cc193   bridge    bridge    local
33ce3a17ab9c   host      host      local
a874dd5dfd0f   mg-net    bridge    local
30be6572ba62   none      null      local
```

I can run [mongo](https://hub.docker.com/_/mongo) docker container and use the network `mg-db` this network. With the
command `docker run` and the flag `network`. get the image for [mongo](https://hub.docker.com/_/mongo) db

```shell
$ docker run -p 27017:27017 --name mgdb -d -e MONGO_INITDB_ROOT_USERNAME=admin -e MONGO_INITDB_ROOT_PASSWORD=admin --network mg-net mongo:5.0.6
Unable to find image 'mongo:5.0.6' locally
5.0.6: Pulling from library/mongo
4d32b49e2995: Pull complete
26a89ffa9c8e: Pull complete
c6a26a1adeb9: Pull complete
0f6c4ca429ae: Pull complete
87cd51bf7ebc: Pull complete
68750eb424ec: Pull complete
008900bad1d7: Pull complete
e33eed19868f: Pull complete
e7bc3cbfdaeb: Pull complete
358eefa21051: Pull complete
Digest: sha256:ad947856db716ddd0b9cc525e341c77208ed8dafcb4a6ad23f9b3addd7a4f71c
Status: Downloaded newer image for mongo:5.0.6
31676add92062aefdfaa69f0a44ce9d3e172b1de65f3c1b70dbcca8403d8b58f
```

We can see now the logs of the container to check if all was successful.

```shell
$ docker logs mgdb
about to fork child process, waiting until server is ready for connections.
forked process: 30
...
```

Now I can run the [mongo expless](https://hub.docker.com/_/mongo-express).

```shell
$ docker run -d -p 8081:8081 --name mongo-express --network mg-net -e ME_CONFIG_MONGODB_ADMINUSERNAME=admin -e ME_CONFIG_MONGODB_ADMINPASSWORD=admin -e ME_CONFIG_MONGODB_SERVER=mgdb mongo-express
Unable to find image 'mongo-express:latest' locally
latest: Pulling from library/mongo-express
6a428f9f83b0: Pull complete
f2b1fb32259e: Pull complete
40888f2a0a1f: Pull complete
4e3cc9ce09be: Pull complete
eaa1898f3899: Pull complete
ab4078090382: Pull complete
ae780a42c79e: Pull complete
e60224d64a04: Pull complete
Digest: sha256:2a25aafdf23296823b06bc9a0a2af2656971262041b8dbf11b40444804fdc104
Status: Downloaded newer image for mongo-express:latest
2992eff3d1128190a217fc179a6fbe48c62caf6188f2c943ce303d76b6060f60
```

Now I can connect to [mongo express](https://hub.docker.com/_/mongo-express) locally and with the
url [http://localhost:8081/](http://localhost:8081/). So far we have a mongo db database server and there web UI. Now I
need to set up my own software example and connect to the mongo db server. Go to the project directory.

```shell
$ cd developing/mongo_flask
```

- set up and activate the python virtual environment.

```shell
$ python3.10 -m venv venv_l
$ source venv_l/bin/activate
(venv_l)
```

- install python requirements.

```shell
$ python -m pip install -r requirements.txt
```

- set up environment variables.

```shell
$ export FLASK_APP=main
$ export FLASK_ENV=development
```

- run the application

```shell
$ flask run
 * Serving Flask app 'main' (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000 (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 103-757-865
```

Now we need to check if the app connect with mongo db. To do that open the
url [http://127.0.0.1:5000](http://127.0.0.1:5000). I need to check the mongo log to debug or understand the
authentication errors.

```shell
$ docker logs mgdb -f
```

With the error identified I need to add new db and user for this db.

```shell
$ docker exec -it mgdb bash
root@37c04bfb4a9c:/#
```

Now I run the mongo client inside the container to crete new database, **users** and ad the user **admin** and some data
to this database.

```shell
root@37c04bfb4a9c:/# mongosh -u admin -p admin
flask_app> use flask_app
flask_app> db.users.insertMany([{name:"Many", age:25}, {name:"Sid", age:24}, {name:"Diego", age:23}, {name:"Ellie", age:22}])
flask_app> show databases
flask_app> db.createUser({user: "admin", pwd: "admin", roles:[{role: "readWrite", db: "flask_app"}]})
```

Now we allow you to use our mongo db with our flask app. we can go or reload
url [http://127.0.0.1:5000](http://127.0.0.1:5000) to check the connection between my flask app and the mongo database.
Now if wan to add some new username we can use the flask app. Make a POST with the data, `{name:"Crash", age:18}` to the
app.